package railfance;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ResourceBundle;

public class UIController implements Initializable {

    @FXML
    private TextArea input;
    @FXML
    private TextArea output;
    @FXML
    private Button execute;
    @FXML
    private TextField height;
    @FXML
    private ComboBox<String> mode;
    @FXML
    private ComboBox<Integer> variants;
    @FXML
    private Label variantsLabel;
    @FXML
    private Button swap;
    @FXML
    private Button clear;

    private RailFenceCipher railFenceCipher;
    private Alert errorAlert;
    private String[] variantsStrings;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mode.getItems().addAll("Шифрування", "Дешифрування", "Криптоаналіз");
        mode.setValue("Шифрування");
    }

    public UIController() {
        railFenceCipher = new RailFenceCipher();
        errorAlert = new Alert(Alert.AlertType.NONE, "", ButtonType.CLOSE);
    }


    @FXML
    private void execute(ActionEvent event) {

        if (!isValidInput()) {
            errorAlert.setContentText("Wrong input");
            errorAlert.showAndWait();
            return;
        }

        if(input.getLength() > 1000) {
            input.setText( input.getText().substring(0,10001));
        }

        try {
            switch (mode.getValue()) {
                case "Шифрування":
                case "Дешифрування":
                    if (isValidHeight()) {
                        int heightCipher = Integer.valueOf(height.getText());
                        String out = (mode.getValue().equals("Шифрування"))
                                ? railFenceCipher.cipher(heightCipher, input.getText())
                                : railFenceCipher.decipher(heightCipher, input.getText());
                        output.setText(out);
                    } else {
                        errorAlert.setContentText("Wrong height");
                        errorAlert.showAndWait();
                        return;
                    }
                    break;
                case "Криптоаналіз":

                    int maxHeight;
                    if (height.getText().equals("")) {
                        maxHeight = input.getLength();
                    } else if (isValidHeight()) {
                        maxHeight = Integer.valueOf(height.getText());
                    } else {
                        maxHeight = input.getLength();
                    }

                    variants.getItems().clear();
                    for (int i = 0; i < maxHeight - 1; i++) {
                        variants.getItems().add(i + 2);
                    }

                    variantsStrings = railFenceCipher.analyse(input.getText(), maxHeight);
                    variants.setValue(2);
                    variants.setVisible(true);
                    variantsLabel.setVisible(true);

                    break;
            }
        } catch (Exception e) {
            new Alert(Alert.AlertType.NONE, "Error: " + e.getMessage(), ButtonType.CLOSE).showAndWait();
        }
    }


    @FXML
    private void changeMode(ActionEvent event) {
        variants.getItems().clear();
        variants.setVisible(false);
        variantsLabel.setVisible(false);
    }

    @FXML
    private void changeVariant(ActionEvent event) {
        if (!variants.getItems().isEmpty()) {
            int i = variants.getValue();
            output.setText(variantsStrings[i - 2]);
        }
    }

    @FXML
    private void swapAreas(ActionEvent event) {
        String temp = input.getText();
        input.setText(output.getText());
        output.setText(temp);
    }

    @FXML
    private void clear(ActionEvent event) {
        output.clear();
        input.clear();
        height.clear();
        variants.setVisible(false);
        variantsLabel.setVisible(false);
    }

    private boolean isValidInput() {
        return input.getLength() > 0;
    }

    private boolean isValidHeight() {
        return height.getHeight() > 0;
    }

}
