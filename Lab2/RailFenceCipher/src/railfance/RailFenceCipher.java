package railfance;

import java.util.Arrays;

public class RailFenceCipher {

    public String cipher(int height, String input) throws Exception {

        if(!isValidInput(height, input)) {
            throw new Exception("Wrong height of cipher");
        }
        String levels[] = new String[height];

        Arrays.fill(levels, "");

        int j = 0;
        boolean direction = true;
        for (int i = 0; i < input.length(); i++) {

            levels[j] += input.charAt(i);

            if (j == height - 1 || j == 0 && i != 0) {
                direction = !direction;
            }
            j = (direction) ? j + 1 : j - 1;
        }

        String result = "";
        for (int i = height - 1; i >= 0; i--) {
            result += levels[i];
        }

        return result;
    }

    public String decipher(int height, String input) throws Exception{

        if(!isValidInput(height, input)) {
            throw new Exception("Wrong height of cipher");
        }

        int[] levelsCount = new int[height];

        int j = 0;
        boolean direction = true;
        for (int i = 0; i < input.length(); i++) {

            levelsCount[j] ++;

            if (j == height - 1 || j == 0 && i != 0) {
                direction = !direction;
            }
            j = (direction) ? j + 1 : j - 1;
        }

        String[] levels = new String[height];
        j = height-1;
        int k = 0;
        while( j >= 0 ) {

            levels[j] = input.substring(k, k+levelsCount[j]);
            k += levelsCount[j];
            j--;
        }

        Arrays.fill(levelsCount, 0);

        j = 0;
        direction = true;
        String result = "";
        for (int i = 0; i < input.length(); i++) {

            result += levels[j].charAt(levelsCount[j]);
            levelsCount[j]++;

            if (j == height - 1 || j == 0 && i != 0) {
                direction = !direction;
            }
            j = (direction) ? j + 1 : j - 1;
        }

        return result;
    }

    public String[] analyse(String input, int maxHeight) throws Exception {

        String[] results = new String[maxHeight-1];

        for(int i=0; i<maxHeight-1; i++) {

            try {
                results[i] = decipher(i+2, input);
            }
            catch (Exception e) {
                throw e;
            }
        }

        return results;
    }

    private boolean isValidInput(int height, String input) {
        return height > 1 && height <= input.length();
    }
}
